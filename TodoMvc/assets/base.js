//var app = angular.module('todo', []);

function TodoCtrl($scope, filterFilter, $location) {

    $scope.todos = [
        {
            name : 'Tâche incomplète',
            completed : false
        },
        {
            name : 'Tâche complète',
            completed : true
        }
    ];

    $scope.statusFilter = {};

    // $scope.remaining = 3;
    $scope.$watch('todos', function(){
        $scope.remaining = filterFilter($scope.todos, {completed:false}).length;
        $scope.allchecked  = !scope.remaining;
    }, true)

    // Filtre par url
    if ($location.path() == '') {
        $location.path('/')
    };
    $scope.location = $location;
    $scope.$watch('location.path', function(path){
        $scope.statusFilter =
            (path == '/active') ? {completed : false} :
            (path == '/active') ? {completed : true} :
            null;
    });

    $scope.removeTodo = function(index) {
        // alert(index);
        $scope.todos.splice(index, 1);
    }

    $scope.addTodo = function() {
        $scope.todos.push({
            name : $scope.newtodo,
            completed : false
        });
        $scope.newtodo = '';
    }

    // $scope.editTodo = function(todo) {
    //     todo.editing = false;
    // }

    $scope.checkAllTodo = function(allchecked){
        $scope.todos.forEach(function(todo){
            todo.completed = allchecked;
        })
    }

}
